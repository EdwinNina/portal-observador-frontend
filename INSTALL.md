## PASOS PARA LA INSTALACION DEL PROYECTO

1. Clonar el repositorio
2. Instalar las dependecias con `npm install`
3. clonar el .env-example a .env.local y configurar las variables de entorno, tomar en cuenta que ambas variables van dirigidas al mismo endpoint
4. Luego ejecutar `npm run dev` para levantar el proyecto