'use server'

import { apiUrl } from "@/constants/apiData";

interface SignInProps {
   email: string;
   password: string;
}

interface SignUpProps {
   username: string;
   email: string;
   password: string;
}

export const signInRequest = async (props: SignInProps) => {
   return await fetch(`${apiUrl}/auth/signin`, {
      method: 'POST',
      headers: {
         'Content-Type': 'application/json'
      },
      body: JSON.stringify(props),
   }).then(res => res.json());
}

export const signUpRequest = async (props: SignUpProps) => {
   return await fetch(`${apiUrl}/auth/signup`, {
      method: 'POST',
      headers: {
         'Content-Type': 'application/json'
      },
      body: JSON.stringify(props),
   }).then(res => res.json());
}