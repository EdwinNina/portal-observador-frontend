'use server'

import { apiUrl } from "@/constants/apiData";
import { AuthResponse } from "@/interfaces/auth-response.interface";
import { CategoriesResponse } from "@/interfaces/categories.interface";
import { HomeResponse, NewResponse, NewsDashboardResponse } from "@/interfaces/home-response.interface";
import { Roles } from "@/interfaces/roles.interface";
import { cookies } from 'next/headers';
import { redirect } from 'next/navigation'
interface PaginationOptions {
   page?: number
   take?: number
   search?: string
   category?: string
   author?: string
}

export const getHomeInformation = async (props: PaginationOptions): Promise<HomeResponse> => {
   let {page = 0, take = 5, search = '', author = '', category = ''} = props
   if(isNaN(Number(page)) || page < 1) page = 0

   const queryParameters = new URLSearchParams({
      page: page.toString(),
      take: take.toString(),
      search,
      author,
      category
   })

   try {
      return await fetch(`${apiUrl}/home?${queryParameters}`, {cache: 'no-store' }).then(resp => resp.json())
   } catch (error) {
      throw new Error('No se puso cargar la informacion de la pagina')
   }
}

export const getOneNew = async (id: string): Promise<NewResponse> => {
   try {
      return await fetch(`${apiUrl}/home/news/${id}`, {cache: 'no-store' }).then(resp => resp.json())
   } catch (error) {
      throw new Error('No se puso cargar la informacion de la pagina')
   }
}

export const getAllNews = async(): Promise<NewsDashboardResponse> => {
   const {jwt, user} = getJwt()
   const url = user.role === Roles.editor ? `${apiUrl}/news/no-approved` : `${apiUrl}/news`

   const data = await fetch(url, {
      headers: { 'Authorization': `Bearer ${jwt}` }
   }).then((response) => response.json())

   return data
}

export const saveNew = async (data: FormData) => {
   const { jwt } = getJwt()

   try {
      const response = await fetch(`${apiUrl}/news`, {
         method: 'POST',
         headers: { 'Authorization': `Bearer ${jwt}` },
         body: data
      }).then((response) => response.json())

      return response
   } catch (error) {
      throw error
   }
}

export const updateNew = async (id: string, data: FormData) => {
   const { jwt } = getJwt()

   try {
      const response = await fetch(`${apiUrl}/news/${id}`, {
         method: 'PATCH',
         headers: { 'Authorization': `Bearer ${jwt}` },
         body: data
      }).then((response) => response.json())

      return response
   } catch (error) {
      throw error
   }
}

export const getCategories = async(): Promise<CategoriesResponse> => {
   return await fetch(`${apiUrl}/categories`).then((response) => response.json())
}

function getJwt() {
   const cookieStore = cookies()
   const payload = cookieStore.get('authStore')
   const payloadData = JSON.parse(payload!.value) as AuthResponse

   return payloadData
}

export const goToEditNewForm = (id: string) => {
   redirect(`/dashboard/noticias/editar/${id}`)
}