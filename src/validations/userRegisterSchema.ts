import { z } from "zod";

export const userRegisterSchema = z.object({
   username: z.string().min(3, {
      message: 'El nombre de usuario debe tener minimo 3 caracteres'
   }),
   email: z.string().email({
      message: 'Ingresa un correo valido'
   }),
   password: z.string().min(6, {
      message: 'La contraseña debe tener minimo 6 caracteres'
   }).regex(/(?:(?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
      message: 'La contraseña debe tener letras mayusculas, minusculas y numeros'
   })
})