import { z } from "zod";

export const userLoginSchema = z.object({
   email: z.string().email({
      message: 'Ingresa un correo valido'
   }),
   password: z.string().min(6, {
      message: 'La contraseña debe tener minimo 6 caracteres'
   }).regex(/(?:(?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
      message: 'La contraseña debe tener letras mayusculas, minusculas y numeros'
   })
})