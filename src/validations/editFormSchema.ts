import { z } from "zod";

export const editFormSchema = z.object({
   title: z.string().min(10, {
      message: 'El titulo debe tener minimo 10 caracteres'
   }),
   place: z.string().min(5, {
      message: 'El lugar debe tener minimo 10 caracteres'
   }),
   content: z.string().min(5, {
      message: 'El contenido debe tener minimo 10 caracteres'
   }),
   category: z.string().min(1, { message: "Selecciona una opción" }),
   image: z.any().optional(),
   approved: z.boolean().optional()
})