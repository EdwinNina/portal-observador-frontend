
export interface CommentsResponse {
   id:       string;
   title:    string;
   comments: Comment[];
}

export interface Comment {
   id:        string;
   createdAt: string;
   comment:   string;
   user:      User;
}

export interface User {
   id:       string;
   username: string;
   email:    string;
   active:   boolean;
   role:     string;
}
