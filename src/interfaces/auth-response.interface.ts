
export interface AuthResponse {
   user: User;
   jwt:  string;
}

export interface User {
   username: string;
   email:    string;
   role:     string;
}
