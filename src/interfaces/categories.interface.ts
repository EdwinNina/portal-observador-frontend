export interface CategoriesResponse {
    data: Category[];
}

export interface Category {
    id:    string;
    title: string;
}
