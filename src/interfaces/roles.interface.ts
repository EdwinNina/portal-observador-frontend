export enum Roles {
   'redactor' = 'redactor',
   'editor' = 'editor',
   'visitante' = 'visitante',
   'suscriptor' = 'suscriptor'
}