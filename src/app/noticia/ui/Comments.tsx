'use client'

import { apiUrl } from "@/constants/apiData";
import { AuthResponse } from "@/interfaces/auth-response.interface";
import { Comment } from "@/interfaces/comment-response.interface";
import { Box, TextField, Button, Typography, Avatar, Grid, Container } from "@mui/material";
import { getCookie } from "cookies-next";
import moment from "moment";
import React, { useEffect } from "react";
import { useState } from 'react';
import 'moment/locale/es';
import Link from "next/link";

export const CommentsSection = ({idNew}: { idNew : string }) => {
   const [comment, setComment] = useState('')
   const [comments, setComments] = useState([])
   const [error, setError] = useState('')
   const [isAuthenticated, setIsAuthenticated] = useState(false)
   const [token, setToken] = useState('')
   const store = getCookie('authStore')
   let subscriptorAuthenticated = null;

   useEffect(() => {
      if(store) {
         const {jwt, user} = JSON.parse(store) as AuthResponse
         subscriptorAuthenticated = user
         setToken(jwt)
         setIsAuthenticated(true)
      }
      fetchComments()
   }, [])

   const fetchComments = async () => {
      try {
        const response = await fetch(`${apiUrl}/news/comments/${idNew}`);
        const data = await response.json();
        setComments(data.comments);
      } catch (error) {
        console.error('Error fetching comments:', error);
      }
   };

   const handleCommit = () => {
      if(comment.trim().length === 0) {
         setError('No puedes comentar con el campo vacio')
         return
      }
      fetch(`${apiUrl}/comments`, {
         method: 'POST',
         headers: { 
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
         },
         body: JSON.stringify({
            comment,
            new: idNew
         })
      }).then(resp => resp.json())
      .then((data) => {
         if(data) {
            setComment('')
            fetchComments()
         }
      })
   }

   return (
		<Box>
         {
            isAuthenticated ? (
               <Box sx={{display: 'flex', gap: 2, marginY: 3}}>
                  <TextField
                     label="Agregar un comentario"
                     variant="outlined"
                     fullWidth
                     onChange={e => setComment(e.target.value)}
                     value={comment}
                     helperText={error}
                     error={Boolean(error)}
                  />
                  <Button variant="contained" color="primary" onClick={handleCommit}>
                     Comentar
                  </Button>
               </Box>
            ) : (
               <Box
                  sx={{ display: 'flex', flexDirection: 'column', gap: 2, marginY: 3, backgroundColor: '#f1f1f1', alignItems: 'center', paddingY: 4 }}
               >
                  <Typography variant="h6">Inicia sesión para comentar</Typography>
                  <Link href='/signIn'>
                     <Button variant="contained">INICIAR SESIÓN</Button>
                  </Link>
               </Box>
            )
         }
			<Box>
				<Typography variant="h6">Comentarios</Typography>
            {
               comments.map((comment: Comment) => (
                  <Box sx={{ marginTop: 2, display:'flex', gap: 3 }} key={comment.id}>
                     <Avatar sx={{ marginLeft: 5 }} src={`https://ui-avatars.com/api/?name=${comment.user.username}`} >H</Avatar>
                     <Box>
                        <Box
                           sx={{
                              display: "flex",
                              gap: 1,
                              justifyContent: "start",
                              alignItems: "center",
                           }}
                        >
                           <Typography variant="h6" sx={{ fontWeight: "500" }}>
                              { comment.user.username }
                           </Typography>
                           <Typography variant="h6" sx={{ fontSize: "15px" }}>
                              { moment(comment.createdAt).fromNow() }
                           </Typography>
                        </Box>
                        <Typography variant="subtitle2">{comment.comment}</Typography>
                     </Box>
                  </Box>
               ))
            }
			</Box>
		</Box>
	);
};
