import { HomeHeroe } from "@/components/homeHeroe/Hero";
import { Navbar } from "@/components/navbar/Navbar";
import { AuthResponse } from "@/interfaces/auth-response.interface";
import { getOneNew } from "@/services/api.service";
import {
	Avatar,
	Box,
	Button,
	Card,
	CardContent,
	Chip,
	Container,
	Divider,
	Stack,
	Typography,
} from "@mui/material";
import Grid2 from "@mui/material/Unstable_Grid2";
import { cookies } from "next/headers";
import Image from "next/image";
import Link from "next/link";
import { CommentsSection } from "../ui/Comments";

interface Props {
	params: {
		id: string;
	};
}

export default async function NewPage({ params }: Props) {
	const { id } = params;
	const newFound = await getOneNew(id);
	const { image, author, category, content, place, title, publishedAt } =
		newFound.data;

	const urlImage = image ?? "https://fakeimg.pl/1000x400";
	const date = new Date(publishedAt);
	const cookieStore = cookies()
	let isAuthenticated = null;
	const store = cookieStore.get('authStore')
 
	if(store) {
		const {user} = JSON.parse(store.value) as AuthResponse
		isAuthenticated = user
	}
 
   return (
		<>
			<Navbar isAuthenticated={isAuthenticated} />
			<HomeHeroe />
			<Container maxWidth="xl">
				<Grid2 container spacing={10}>
					<Grid2 xs={8}>
						<Typography variant="h3" sx={{ marginTop: "40px" }}>
							{title}
						</Typography>
						<Typography
							variant="h6"
							sx={{ marginY: "10px", textTransform: "uppercase" }}
						>
							{place}
						</Typography>
						<Divider />
						<Box
							sx={{
								display: "flex",
								justifyContent: "space-between",
								padding: 4,
							}}
						>
							<Stack direction="row" spacing={1}>
								<Chip label={category.title} color="primary" />
							</Stack>
							<Typography variant="h5">
								{date.toLocaleDateString("es-ES", {
									year: "numeric",
									month: "long",
									day: "numeric",
									hour: "2-digit",
									minute: "2-digit",
								})}
							</Typography>
						</Box>
						<Divider />
						<Image  src={urlImage} alt={title} width={950} height={400} style={{ objectFit: 'contain', maxWidth: '100%' }} />
						<Typography variant="subtitle1">{content}</Typography>
						<CommentsSection idNew={id} />
					</Grid2>
					<Grid2 xs={4}>
						<Card sx={{ marginTop: 10 }}>
							<CardContent
								sx={{
									display: "flex",
									flexDirection: "column",
									alignItems: "center",
								}}
							>
                     <Typography variant="h4">Autor</Typography>
								<Avatar
									alt="Remy Sharp"
									src={`https://ui-avatars.com/api/?name=${author.username}`}
									sx={{ marginY: 4, width: 100, height: 100 }}
								/>
								<Typography
									variant="h5"
									sx={{ textTransform: "uppercase" }}
									color="text.secondary"
									gutterBottom
								>
									{author.username}
								</Typography>
								<Typography sx={{ mb: 1.5 }} color="text.secondary">
									{author.email}
								</Typography>
							</CardContent>
						</Card>
                  <Link href='/'>
                     <Button variant="contained" sx={{ marginTop: 4}}>
                        Volver
                     </Button>
                  </Link>
					</Grid2>
				</Grid2>
			</Container>
		</>
	);
}
