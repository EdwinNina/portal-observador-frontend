'use client'

import { Box, TextField, Button, Grid } from "@mui/material";
import Link from "next/link";
import { SubmitHandler, useForm } from 'react-hook-form'
import { zodResolver } from '@hookform/resolvers/zod';
import { userLoginSchema } from "@/validations/userLoginSchema";
import { signInRequest } from "@/services/auth.service";
import Swal from 'sweetalert2'
import { useRouter } from "next/navigation";
import { Roles } from "@/interfaces/roles.interface";
import { setCookie } from "cookies-next";
import { AuthResponse } from "@/interfaces/auth-response.interface";

type Inputs = {
   email: string;
   password: string;
}

export const LoginForm = () => {
   const router = useRouter()
   const {register, handleSubmit, formState: {errors}} = useForm<Inputs>({
      resolver: zodResolver(userLoginSchema)
   });

   const onSubmit: SubmitHandler<Inputs> = async (data) => {
      const response = await signInRequest(data)

      if(response.statusCode === 401) {
         Swal.fire({
            title: 'Error!',
            text: response.message,
            icon: 'error',
         })
         return
      }
      const authResponse = response as AuthResponse

      setCookie('authStore', authResponse, { sameSite: 'strict' })

      if([Roles.editor, Roles.redactor].includes(response.user.role)) {
         router.push('/dashboard/noticias')
      } else {
         router.push('/')
      }
      window.location.reload()
   }

   return (
      <Box
         component="form"
         noValidate
         sx={{ mt: 1, paddingX: 10 }}
         onSubmit={handleSubmit(onSubmit)}
      >
         <TextField
            margin="normal"
            required
            fullWidth
            id="email"
            label="Correo electrónico"
            autoFocus
            {...register('email')}
            helperText={errors.email?.message}
            error={Boolean(errors.email)}
         />
         <TextField
            margin="normal"
            required
            fullWidth
            label="Contraseña"
            type="password"
            id="password"
            {...register('password')}
            helperText={errors.password?.message}
            error={Boolean(errors.password)}
         />
         <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
         >
            Iniciar Sesión
         </Button>
         <Grid container>
            <Grid item>
               <Link href="/signUp">
                  No tienes una cuenta? Registrate
               </Link>
            </Grid>
         </Grid>
      </Box>
   );
};
