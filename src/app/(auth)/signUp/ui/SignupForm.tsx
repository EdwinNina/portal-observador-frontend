'use client'

import { signUpRequest } from "@/services/auth.service";
import { userRegisterSchema } from "@/validations/userRegisterSchema";
import { zodResolver } from "@hookform/resolvers/zod";
import { Box, TextField, Button, Grid } from "@mui/material";
import { setCookie } from "cookies-next";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { SubmitHandler, useForm } from "react-hook-form";
import Swal from "sweetalert2";

type Inputs = {
   username: string;
   email: string;
   password: string;
}

export const SignUpForm = () => {
   const router = useRouter()
   const {register, handleSubmit, formState: {errors}} = useForm<Inputs>({
      resolver: zodResolver(userRegisterSchema)
   });

   const onSubmit: SubmitHandler<Inputs> = async (data) => {
      const response = await signUpRequest(data)

      if(response.statusCode === 401) {
         Swal.fire({
            title: 'Error!',
            text: response.message,
            icon: 'error',
         })
         return
      }
      setCookie('subscriptorToken', response, { sameSite: 'strict' })
      router.push('/')
   }

   return (
		<Box
         component="form"
         noValidate sx={{ mt: 1, paddingX: 10 }}
         onSubmit={handleSubmit(onSubmit)}
      >
			<TextField
				margin="normal"
				required
				fullWidth
				id="username"
				label="Nombre de Usuario"
				autoFocus
            {...register('username')}
            helperText={errors.username?.message}
            error={Boolean(errors.username)}
			/>
			<TextField
				margin="normal"
				required
				fullWidth
				id="email"
				label="Correo electrónico"
            {...register('email')}
            helperText={errors.email?.message}
            error={Boolean(errors.email)}
			/>
			<TextField
				margin="normal"
				required
				fullWidth
				label="Contraseña"
				type="password"
				id="password"
            {...register('password')}
            helperText={errors.password?.message}
            error={Boolean(errors.password)}
			/>
			<Button type="submit" fullWidth variant="contained" sx={{ mt: 3, mb: 2 }}>
				Registrarse
			</Button>
			<Grid container>
				<Grid item>
					<Link href="/signIn">Ya tienes una cuenta? Inicia Sesión</Link>
				</Grid>
			</Grid>
		</Box>
	);
};
