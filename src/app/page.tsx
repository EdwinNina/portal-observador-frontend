import { Container, Typography } from "@mui/material";
import Grid2 from "@mui/material/Unstable_Grid2/Grid2";
import { Navbar } from "@/components/navbar/Navbar";
import { HomeHeroe } from "@/components/homeHeroe/Hero";
import { getHomeInformation } from "@/services/api.service";
import { PaginationComp } from "@/components/pagination/Pagination";
import { NewsGrid } from "@/components/news/NewsGrid";
import { SearchHome } from "@/components/search/SearchHome";
import { FiltroHome } from "@/components/filtros/FiltroHome";
import { cookies } from "next/headers";
import { AuthResponse } from "@/interfaces/auth-response.interface";

interface Props {
  searchParams: {
    page?: string
    search?: string
    category?: string
    author?: string 
  }
}

export default async function Home({searchParams} :Props) {
  const page = searchParams.page ? parseInt(searchParams.page) : 0
  const {authors, categories, news} = await getHomeInformation({
    page,
    category: searchParams.category,
    author: searchParams.author,
    search: searchParams.search
  });
  const cookieStore = cookies()
  let isAuthenticated = null;
  const store = cookieStore.get('authStore')

  if(store) {
    const {user} = JSON.parse(store.value) as AuthResponse
    isAuthenticated = user
  }

  return (
    <>
      <Navbar isAuthenticated={isAuthenticated} />
      <HomeHeroe/>
      <Container maxWidth='xl'>
        <Typography
          variant="h3"
          sx={{ marginY: '40px' }}
        >
          Noticias
        </Typography>
        <Grid2 container spacing={3}>
          <Grid2 xs={3}>
            <SearchHome />
            <Typography
              variant="h5"
              sx={{ marginY: '40px' }}
            >
              Filtrar por
            </Typography>
            <FiltroHome authors={authors} categories={categories} />
          </Grid2>
          <Grid2 xs={9} container spacing={{ xs: 2, md: 3 }} columns={{ xs: 1, sm: 8, md: 12 }}>
            {
              (news.data.length === 0)
              ? (
                  <Grid2 container justifyContent='center' sx={{ width: '100%' }}>
                    <Typography variant="h4">No hay noticias registradas</Typography>
                  </Grid2>
                )
              : (
                <>
                  <NewsGrid news={news.data} />
                  <PaginationComp totalPages={news.totalPages} />
                </>
              )
            }
          </Grid2>
        </Grid2>
      </Container>
    </>
  );
}
