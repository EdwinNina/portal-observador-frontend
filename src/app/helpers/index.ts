import { Roles } from "@/interfaces/roles.interface";

export function getRoleFromString(roleString: string): Roles {
   switch (roleString) {
     case 'redactor':
       return Roles.redactor;
     case 'editor':
       return Roles.editor;
     case 'visitante':
       return Roles.visitante;
     case 'suscriptor':
       return Roles.suscriptor;
     default:
       throw new Error(`Role desconocido: ${roleString}`);
   }
}