import type { Metadata } from "next";
import { Lato } from "next/font/google";
import "./globals.css";

const inter = Lato({ weight: "400", subsets: ["latin"]});

export const metadata: Metadata = {
  title: "Noticias el Observador",
  description: "Este es un portal de noticias llamado el observador",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className} style={{margin: 0}}>{children}</body>
    </html>
  );
}
