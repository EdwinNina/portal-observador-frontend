'use client'

import { New } from "@/interfaces/home-response.interface";
import { DataGrid, GridActionsCellItem, GridColDef, GridRowId } from "@mui/x-data-grid"
import EditIcon from '@mui/icons-material/Edit';
import { Avatar, Chip } from "@mui/material";
import { goToEditNewForm } from "@/services/api.service";

interface Props {
   news: New[]
}

const columns: GridColDef[] = [
   { field: 'id', headerName: 'ID', width: 150},
   { field: 'title', headerName: 'Titulo', width: 250 },
   { field: 'place', headerName: 'Lugar', width: 150 },
   { field: 'publishedAt', headerName: 'Fecha', width: 200 },
   { field: 'category', headerName: 'Categoria', width: 150 },
   { field: 'content', headerName: 'Contenido', width: 300 },
   { 
      field: 'image',
      headerName: 'Imagen',
      renderCell: (params) => (
         <Avatar alt={params.row.title} src={params.row.image} />
      )
   },
   { 
      field: 'approved',
      headerName: 'Estado',
      width: 130,
      renderCell: (params) => (
         <Chip
            label={params.row.approved ? 'Aprobado': 'Sin aprobar'}
            color={params.row.approved ? 'success': 'default'}
         />
      )
   },
   {
      field: 'actions',
      type: 'actions',
      headerName: 'Actions',
      width: 100,
      cellClassName: 'actions',
      getActions: ({ id }) => {
        return [
          <GridActionsCellItem
            icon={<EditIcon />}
            label="Edit"
            className="textPrimary"
            onClick={() => goToEditNewForm(id as string)}
            color="inherit"
          />
        ];
      }
   }
];

export const NewsTable = ({ news }: Props) => {

   const rows = news.map((item: New) => ({
      id: item.id,
      title: item.title,
      place: item.place,
      publishedAt: item.publishedAt,
      category: item.category.title,
      content: item.content,
      image: item.image ?? "https://fakeimg.pl/1000x400"
   }));

   return (
      <DataGrid
         rows={rows}
         columns={columns}
         initialState={{
            pagination: {
               paginationModel: { page: 0, pageSize: 5 },
            },
         }}
         pageSizeOptions={[5, 10]}
         checkboxSelection
      />
   )
}
