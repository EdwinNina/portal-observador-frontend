'use client'

import { AppBar, Box, Typography, Button } from "@mui/material";
import { useRouter } from "next/navigation";
import { deleteCookie } from "cookies-next";

export const NavbarDashboard = () => {
   const router = useRouter()

   const logout = () => {
      deleteCookie('authStore')
      router.push('/')
   }

	const goToHome = () => {
		router.push('/')
	}

   return (
		<AppBar color="info" sx={{ px: 4, py: 3 }}>
			<Box sx={{ display: "flex", justifyContent: "space-between" }}>
				<Typography variant="h6">Panel Administrativo</Typography>
				<Box>
					<Button color="inherit" onClick={goToHome} sx={{ color: 'white'}}>
						Home
					</Button>
					<Button color="inherit" onClick={logout}>Logout</Button>
				</Box>
			</Box>
		</AppBar>
	);
};
