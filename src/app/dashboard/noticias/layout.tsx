import * as React from "react";
import Container from "@mui/material/Container";
import { Box, Grid, Paper } from "@mui/material";
import { NavbarDashboard } from "../ui/NavbarDashboard";

export default function DashboardLayout({
	children,
}: Readonly<{ children: React.ReactNode }>) {
	return (
		<Box>
			<NavbarDashboard/>
			<Box sx={{ marginTop: 15 }} />
			<Container maxWidth='xl'>
				<Grid item xs={12}>
					<Paper sx={{ p: 2, display: "flex", flexDirection: "column" }}>
						{children}
					</Paper>
				</Grid>
			</Container>
		</Box>
	);
}
