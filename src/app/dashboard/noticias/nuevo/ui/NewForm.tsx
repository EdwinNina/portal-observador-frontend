'use client'

import { saveNew } from "@/services/api.service";
import { newFormSchema } from "@/validations/newFormSchema";
import { zodResolver } from "@hookform/resolvers/zod";
import { Box, TextField, FormControl, InputLabel, Select, MenuItem, FormHelperText, Button } from "@mui/material";
import { useState } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { styled } from '@mui/material/styles';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import { Category } from "@/interfaces/categories.interface";
import { useRouter } from "next/navigation";
import Swal from "sweetalert2";

type Inputs = {
   title: string
   place: string
   category: string
   content: string
   image?: FileList
}

const VisuallyHiddenInput = styled('input')({
   clip: 'rect(0 0 0 0)',
   clipPath: 'inset(50%)',
   height: 1,
   overflow: 'hidden',
   position: 'absolute',
   bottom: 0,
   left: 0,
   whiteSpace: 'nowrap',
   width: 1,
});

interface Props {
	categories: Category[];
}

export const NewForm = ({categories}: Props) => {
   const [category, setCategory] = useState('')
	const router = useRouter()
   const {register, handleSubmit, formState: {errors}} = useForm<Inputs>({
      resolver: zodResolver(newFormSchema)
   });

   const onSubmit: SubmitHandler<Inputs> = async (data) => {
		const formData = new FormData()
		formData.append('title', data.title)
		formData.append('place', data.place)
		formData.append('category', data.category)
		formData.append('content', data.content)

		if(data.image && data.image?.length > 0) {
			formData.append('image', data.image[0])
		}

		const response = await saveNew(formData)

		if(response.error) {
			Swal.fire({
            title: 'Error!',
            text: response.message,
            icon: 'error',
         })
			return
		}
		Swal.fire({
			icon: "success",
			title: "La noticia se guardo correctamente",
			showConfirmButton: false,
			timer: 1500
		});
		if(response.data.title) router.push('/dashboard/noticias')
   }

	return (
		<Box
			component="form"
			noValidate
			autoComplete="off"
			onSubmit={handleSubmit(onSubmit)}
			encType="multipart/form-data"
		>
			<TextField
				label="Title"
				fullWidth
				margin="normal"
				{...register("title")}
				helperText={errors.title?.message}
				error={Boolean(errors.title)}
			/>
			<TextField
				label="Place"
				fullWidth
				margin="normal"
				{...register("place")}
				helperText={errors.place?.message}
				error={Boolean(errors.place)}
			/>
			<FormControl fullWidth margin="normal" error={!!errors.category}>
				<InputLabel id="category-label">Category</InputLabel>
				<Select
					id="category-select"
					{...register("category")}
					value={category}
					onChange={(e) => setCategory(e.target.value)}
				>
					<MenuItem value="">Selecciona una opción</MenuItem>
					{categories.map(({ id, title }) => (
						<MenuItem value={id} key={id}>
							{title}
						</MenuItem>
					))}
				</Select>
				{errors.category && (
					<FormHelperText>{errors.category?.message}</FormHelperText>
				)}
			</FormControl>
			<TextField
				label="Content"
				multiline
				rows={4}
				variant="outlined"
				fullWidth
				margin="normal"
				{...register("content")}
				helperText={errors.content?.message}
				error={Boolean(errors.content)}
			/>
			<Button
				component="label"
				role={undefined}
				variant="contained"
				tabIndex={-1}
				startIcon={<CloudUploadIcon />}
				sx={{ marginTop: 3 }}
			>
				Subir imagen
				<VisuallyHiddenInput type="file" {...register("image")} />
			</Button>
			<Box sx={{ display: "flex", marginY: 5 }}>
				<Button type="submit" variant="outlined" fullWidth>
					Guardar
				</Button>
			</Box>
		</Box>
	);
};
