import { Typography, Box, IconButton, Tooltip } from "@mui/material";
import Grid2 from "@mui/material/Unstable_Grid2/Grid2";
import React from "react";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import Link from "next/link";
import { NewForm } from "./ui/NewForm";
import { getCategories } from "@/services/api.service";

export default async function FormNew() {
	const { data } = await getCategories();

   return (
		<Grid2 maxWidth={1000} margin="auto">
			<Box sx={{ display: "flex", justifyContent: "space-between" }}>
				<Typography>Formulario de Ingreso de Noticias</Typography>
				<Link href="/dashboard/noticias">
					<Tooltip title="Volver al Listado">
						<IconButton color="primary" aria-label="volver">
							<ArrowBackIcon />
						</IconButton>
					</Tooltip>
				</Link>
			</Box>
         <NewForm categories={data} />
		</Grid2>
	);
}
