import { getCategories, getOneNew } from "@/services/api.service";
import { Box, Typography, Tooltip, IconButton } from "@mui/material";
import Grid2 from "@mui/material/Unstable_Grid2";
import Link from "next/link";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { EditForm } from "../ui/EditForm";

interface Props {
	params: {
		id: string;
	};
}

export default async function EditarNoticia({params}: Props) {
   const { id } = params;
   const { data: categories } = await getCategories();
   const { data } = await getOneNew(id)

   return (
      <Grid2 maxWidth={1000} margin="auto">
         <Box sx={{ display: "flex", justifyContent: "space-between" }}>
            <Typography>Formulario de Edición de Noticias</Typography>
            <Link href="/dashboard/noticias">
               <Tooltip title="Volver al Listado">
                  <IconButton color="primary" aria-label="volver">
                     <ArrowBackIcon />
                  </IconButton>
               </Tooltip>
            </Link>
         </Box>
         <EditForm categories={categories} newData={data} />
      </Grid2>
   )
}
