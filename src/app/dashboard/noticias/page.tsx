import { getAllNews } from "@/services/api.service"
import { Box, Button, Typography } from "@mui/material";
import { NewsTable } from "../ui/NewsTable";
import Link from "next/link";
import { cookies } from "next/headers";
import { AuthResponse } from "@/interfaces/auth-response.interface";
import { Roles } from "@/interfaces/roles.interface";

export default async function DashboardPage() {
   const {news} = await getAllNews()
   const cookieStore = cookies()
   let isRedactor: boolean = false;
   const store = cookieStore.get('authStore')

   if(store) {
      const {user} = JSON.parse(store.value) as AuthResponse
      if(user.role === Roles.redactor) {
         isRedactor = true
      }
   }
   return (
      <Box>
         <Box sx={{ display: 'flex', justifyContent: 'space-between', marginBottom: 3, marginTop: 2}}>
            <Typography>Listado de Noticias</Typography>
            {
               isRedactor && (
                  <Link href='/dashboard/noticias/nuevo'>
                     <Button type="button" variant="outlined">Agregar</Button>
                  </Link>
               )
            }
         </Box>
         <NewsTable news={news}/>
      </Box>
   )
}