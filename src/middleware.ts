import { cookies } from 'next/headers'
import { NextResponse } from 'next/server'
import type { NextRequest } from 'next/server'
import { AuthResponse } from './interfaces/auth-response.interface'
import { jwtVerify } from 'jose'
import { Roles } from './interfaces/roles.interface'
import { getRoleFromString } from './app/helpers'

export async function middleware(request: NextRequest) {
  const cookieStore = cookies()
  const requestedUrl = new URL(request.nextUrl.clone());

  const payload = cookieStore.get('authStore')

  if(payload) {
    const {user, jwt} = JSON.parse(payload!.value) as AuthResponse

    if(user.role === Roles.suscriptor) {
      if(['/signIn', '/signUp', '/dashboard/noticias'].includes(requestedUrl.pathname)) {
        return NextResponse.redirect(new URL('/', request.url))
      }
    }

    const userRole = getRoleFromString(user.role)
  
    if([Roles.redactor, Roles.editor].includes(userRole)) {
      if(requestedUrl.pathname === '/signIn' || requestedUrl.pathname === '/signUp') {
        return NextResponse.redirect(new URL('/dashboard/noticias', request.url))
      }
    }
  
    try {
      const {payload} = await jwtVerify(jwt, new TextEncoder().encode('S3CR37P4SSW0RD'))
      return NextResponse.next()
    } catch (error) {
      return NextResponse.redirect(new URL('/signIn', request.url))
    }
  }
  return NextResponse.next()
}

export const config = {
  matcher: ['/dashboard/noticias', '/signIn', '/signUp']
}