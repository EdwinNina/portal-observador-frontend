
interface menuItemInterface {
   path: string
   label: string
}

export const menuItems: menuItemInterface[] = [
   {  path: '/signIn', label: 'Iniciar Sesion' },
   {  path: '/signUp', label: 'Registrarse' },
]