'use client'

import { Author, Category } from "@/interfaces/home-response.interface";
import { FormControl, FormLabel, RadioGroup, FormControlLabel, Radio, Typography, InputLabel, Select, MenuItem, Button } from "@mui/material";
import { useState } from "react";
import { useRouter } from "next/navigation";

interface Props {
   categories: Category[],
   authors: Author[]
}

export const FiltroHome = ({authors, categories }: Props) => {
   const router = useRouter()
   const [filter, setFilter] = useState({
      category: '',
      author: ''
   })

   const handleFilterReset = () => {
      setFilter({
         category: '',
         author: ''
      })
      router.push('/')
   }

   const handleFilter = () => {
      const {author, category} = filter
      if(!author && !category) return

      let queryParameter = '/?'
      if(author) queryParameter += `author=${author}&`
      if(category) queryParameter += `category=${category}`

      queryParameter = queryParameter.endsWith("&") 
         ? queryParameter.slice(0, -1)
         : queryParameter

      router.push(queryParameter)
   }

   return (
		<>
			<FormControl>
				<FormLabel id="demo-radio-buttons-group-label">Categorias</FormLabel>
				<RadioGroup
					aria-labelledby="demo-radio-buttons-group-label"
					defaultValue="female"
					name="radio-buttons-group"
               value={filter.category || null}
               onChange={
                  (e) => setFilter((state) => ({...state, category: e.target.value}))
               }
				>
					{categories.map(({ id, title }) => (
						<FormControlLabel
							value={id}
							control={<Radio />}
							label={title}
							key={id}
						/>
					))}
				</RadioGroup>
			</FormControl>
			<Typography variant="h5" sx={{ marginY: "40px" }}>
				Autores
			</Typography>
			<FormControl size="medium" fullWidth>
				<InputLabel id="demo-simple-select-label">Seleccionar</InputLabel>
				<Select
					labelId="demo-simple-select-label"
					id="demo-simple-select"
					label="Autores"
               value={filter.author || ''}
               onChange={
                  (e) => setFilter((state) => ({...state, author: e.target.value }))
               }
				>
					{authors.map(({ id, username }) => (
						<MenuItem value={id} key={id}>
							{username}
						</MenuItem>
					))}
				</Select>
			</FormControl>
			<Button
				variant="contained"
				color="primary"
				fullWidth
				sx={{ marginTop: 10 }}
            onClick={handleFilter}
			>
				Filtrar
			</Button>
			<Button
				variant="contained"
				color="inherit"
				fullWidth
				sx={{ marginTop: 1 }}
            onClick={handleFilterReset}
			>
				Limpiar Filtro
			</Button>
		</>
	);
};
