'use client'

import { Box, Pagination } from "@mui/material";
import Grid2 from "@mui/material/Unstable_Grid2";
import { useRouter } from "next/navigation";
import { ChangeEvent } from 'react';

interface Props {
   totalPages: number;
}

export const PaginationComp = ({totalPages}: Props) => {
   const router = useRouter()

   const handleChange = (event: ChangeEvent<unknown>, value: number) => {
      router.push(`/?page=${value.toString()}`);
   };

    return (
		<Grid2 xs={12}>
			<Box sx={{ display: "flex", justifyContent: "center", marginY: 5 }}>
				<Pagination count={totalPages} color="primary" onChange={handleChange} />
			</Box>
		</Grid2>
	);
};
