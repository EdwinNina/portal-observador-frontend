'use client'

import {
	AppBar,
	Box,
	Button,
	Typography,
} from "@mui/material";
import Link from "next/link";
import { menuItems } from "@/constants/menuItems";
import { deleteCookie } from "cookies-next";
import { useRouter } from "next/navigation";
import { User } from "@/interfaces/auth-response.interface";
import { getRoleFromString } from "@/app/helpers";
import { Roles } from "@/interfaces/roles.interface";

export const Navbar = ({ isAuthenticated: user }: {isAuthenticated: User|null}) => {
	const router = useRouter()
	let userRole = Roles.visitante

	if(user) {
		userRole = getRoleFromString(user.role);
	}

	const handleLogout = () => {
		deleteCookie('authStore')
		router.push('/')
		window.location.reload()
	}

	return (
		<Box>
			<AppBar component="nav" elevation={0} color="default">
				<Box sx={{ display: "flex", justifyContent: 'center', padding: 3, flexDirection: 'column', alignItems: 'center'}}
				>
					<Typography
						variant="h3"
						component="div"
						sx={{
							flexGrow: 1,
							display: {
								xs: "none",
								sm: "block",
								color: "#1976d2",
								fontWeight: "700",
							},
						}}
					> EL OBSERVADOR
					</Typography>
					<Box sx={{ display: { xs: "none", sm: "block" } }}>
						{
							user
								? (<Button onClick={handleLogout}>Logout</Button>)
								: 	menuItems.map((menu) => (
										<Link key={menu.path} href={menu.path}>
											<Button>{menu.label}</Button>
										</Link>
									))
						}
						{
							[Roles.redactor, Roles.editor].includes(userRole)
							&& (
								<Link
									href='/dashboard/noticias'
								>
									<Button>Dashboard</Button>
								</Link>
							)
						}
					</Box>
				</Box>
			</AppBar>
		</Box>
	);
};
