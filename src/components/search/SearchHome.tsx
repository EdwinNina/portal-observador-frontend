'use client'

import { useState } from "react"
import { Search } from "@mui/icons-material"
import { TextField, Button } from "@mui/material"
import Grid2 from "@mui/material/Unstable_Grid2"
import { useRouter } from "next/navigation"

export const SearchHome = () => {
   const router = useRouter()
   const [search, setSearch] = useState('')

   const handleSearch = () => {
      if(search.trim().length === 0) return
      router.push(`/?search=${search}`);
   }

   return (
      <Grid2 container columnGap={4}>
         <Grid2>
            <TextField
               label="Buscar"
               variant="outlined"
               fullWidth
               onChange={e => setSearch(e.target.value)}
            />
         </Grid2>
         <Grid2>
            <Button
               variant="contained"
               color="primary"
               startIcon={ <Search /> }
               onClick={handleSearch}
            >
               Buscar
            </Button>
         </Grid2>
      </Grid2>
   )
}
