'use client'

import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import { styled } from '@mui/material/styles';

const backgroundImage = "https://images.pexels.com/photos/3944463/pexels-photo-3944463.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1";

const ProductHeroLayoutRoot = styled('section')(({ theme }) => ({
   color: theme.palette.common.white,
   position: 'relative',
   display: 'flex',
   alignItems: 'center',
   [theme.breakpoints.up('sm')]: {
      height: '50vh',
      minHeight: 500,
      maxHeight: 1300,
   },
}));

const Background = styled('div')({
   position: 'absolute',
   left: 0,
   right: 0,
   top: 0,
   bottom: 0,
   backgroundSize: 'cover',
   backgroundRepeat: 'no-repeat',
   zIndex: -2,
});

export const HomeHeroe = () => {
	return (
      <ProductHeroLayoutRoot>
         <Container
            sx={{
               mt: 3,
               mb: 14,
               display: 'flex',
               flexDirection: 'column',
               alignItems: 'center',
            }}
         >
         <Box
            sx={{
               position: 'absolute',
               left: 0,
               right: 0,
               top: 0,
               bottom: 0,
               backgroundColor: 'common.black',
               opacity: 0.5,
               zIndex: -1,
            }}
         />
         <Background sx={{ backgroundImage: `url(${backgroundImage})`, backgroundPosition: 'center' }} />
         </Container>
      </ProductHeroLayoutRoot>
	);
};