import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Button, CardActionArea, CardActions, Chip } from '@mui/material';
import { New } from '@/interfaces/home-response.interface';
import Link from 'next/link';

interface Props {
   newItem: New
}

export const CardNews = ({newItem}: Props) => {
   const {title, place, category, content, image, id} = newItem

   const newImage = image ?? "https://fakeimg.pl/1000x400" 

   return (
      <Card sx={{ maxWidth: 345 }}>
         <CardActionArea>
         <CardMedia
            component="img"
            height="140"
            sx={{ objectFit: 'cover' }}
            image={newImage}
            alt={title}
         />
         <CardContent>
            <Typography gutterBottom variant="h6" component="div">
               {title} - <Typography fontSize={15}>({place})</Typography>
            </Typography>
            <Chip label={ category.title } />
            <Typography 
               variant="body2" color="text.secondary" 
               sx={{marginTop: 2, textOverflow: 'ellipsis', overflow: 'hidden', whiteSpace: 'nowrap' }}>
               { content }
            </Typography>
         </CardContent>
         </CardActionArea>
         <CardActions>
         <Link href={`/noticia/${id}`}>
            <Button size="small" color="primary">
               Ver más
            </Button>
         </Link>
         </CardActions>
      </Card>
   )
}
