import Grid2 from '@mui/material/Unstable_Grid2';
import { CardNews } from '../card/CardNews';
import { New } from '@/interfaces/home-response.interface';

interface Props {
   news: New[]
}

export const NewsGrid = ({ news }: Props) => {
   return (
      <>
         {
            news.map((newItem) => (
               <Grid2 xs={2} sm={4} md={4} key={newItem.id}>
                  <CardNews newItem={newItem} />
               </Grid2>
            ))
         }
      </>
   )
}